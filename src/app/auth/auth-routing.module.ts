import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { AuthComponent } from './auth.component';
import {RedirectFromLogin} from "../guards/RedirectFromLogin";

const routes: Routes = [
  {path: '', canActivate: [RedirectFromLogin], component: AuthComponent, children : [
    {path: 'login', component : LoginComponent}
  ]}
];

@NgModule({
  imports : [RouterModule.forChild(routes)],
  exports : [RouterModule]
})
export class AuthRoutingModule {}
