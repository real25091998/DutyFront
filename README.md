To install all packages use "npm i"

To start app use "npm run start"

Routs:
  localhost:4200/login - login page;
  localhost:4200/page/workers-list - page with workers list;
  localhost:4200/page/add-worker - page to add worker;
  localhost:4200/page/update-worker/:id - page to update worker;
  localhost:4200/page/worker/:id - page to show info about worker;
  localhost:4200/page/fired-workers-list - page with fired workers list;
  localhost:4200/page/tasks-list - page with tasks list;
  localhost:4200/page/add-task - page to add task;
  localhost:4200/page/update-task/:id - page to update task;
  localhost:4200/page/duty/:id - page to show duty dates of worker;
